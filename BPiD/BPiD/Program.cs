﻿using System;

namespace BPiD
{
    public class Program
    {
        public static void Main(string[] args)
        {
            while (true)
            {
                DisplayGreetings();

                var key = Console.ReadKey(true);

                switch (key.KeyChar)
                {
                    case '1':
                        CountFrequencies();
                        break;
                    case '2':
                        CaesarCipher();
                        break;
                    case '3':
                        CaesarCipherHacking();
                        break;
                    case '4':
                        OneTimePad();
                        break;
                    case '5':
                        Xor();
                        break;
                    case '6':
                        return;
                    default:
                        Console.WriteLine("Nice try.");
                        break;
                }
            }
        }

        private static void DisplayGreetings()
        {
            Console.Clear();
            Console.WriteLine("Type 1 to select task 1.1: counting frequencies of letters in different corpora.");
            Console.WriteLine("Type 2 to select task 1.2: the Caesar cipher encryption.");
            Console.WriteLine("Type 3 to select task 1.3: the Caesar cipher hacking.");
            Console.WriteLine("Type 4 to select task 2.1: one-time pad.");
            Console.WriteLine("Type 5 to select task 2.2: XOR.");
            Console.WriteLine("Type 6 to exit.");
        }

        private static void CountFrequencies()
        {
            var fictionLiteratureLettersFrequencies = FrequencyDictionary.CreateFromFiles("../../Data/fiction");
            var scientificLiteratureLettersFrequencies = FrequencyDictionary.CreateFromFiles("../../Data/science");
            var bibleLettersFrequencies = FrequencyDictionary.CreateFromFiles("../../Data/bible");

            Console.WriteLine("Fiction:\n{0}\n", fictionLiteratureLettersFrequencies);
            Console.WriteLine("Scientific literature:\n{0}\n", scientificLiteratureLettersFrequencies);
            Console.WriteLine("Holy Bible:\n{0}\n", bibleLettersFrequencies);
            Console.ReadKey(true);
        }

        private static void OneTimePad()
        {
            Console.WriteLine("Type 0 for auto-generated key or 1 to enter key by yourself:");
            var action = Console.ReadLine();
            OneTimePad oneTimePad;

            switch (action)
            {
                case "0":
                    oneTimePad = new OneTimePad();
                    oneTimePad.PrintKey();
                    break;
                case "1":
                    Console.WriteLine("Enter key:");
                    oneTimePad = new OneTimePad(Console.ReadLine());
                    break;
                default:
                    return;
            }

            Console.WriteLine("Enter text:");
            var text = Console.ReadLine();
            var ecryptedText = oneTimePad.Encrypt(text);
            Console.WriteLine($"Encrypted: {ecryptedText}");
            Console.WriteLine($"Decrypted: {oneTimePad.Decrypt(ecryptedText)}");
            Console.ReadKey(true);
        }

        private static void CaesarCipher()
        {
            Console.WriteLine("Enter shift:");
            var caesarCipher = new CaesarCipher(Convert.ToInt32(Console.ReadLine()));

            Console.WriteLine("Type 0 for encryption or 1 for decryption:");
            var action = Console.ReadLine();

            Console.WriteLine("Enter text:");
            var text = Console.ReadLine();

            switch (action)
            {
                case "0":
                    Console.WriteLine(caesarCipher.Encrypt(text));
                    break;
                case "1":
                    Console.WriteLine(caesarCipher.Decrypt(text));
                    break;
                default:
                    return;
            }

            Console.ReadKey(true);
        }

        private static void CaesarCipherHacking()
        {
            Console.WriteLine("Enter text:");
            var analyzer = new CaesarCipherAnalyzer();

            var data = Console.ReadLine();
            analyzer.DecypherBruteforce(data);
            analyzer.DecypherWithTopK(data);

            Console.ReadKey(true);
        }


        private static void Xor()
        {
            Console.WriteLine("Enter two integers:");
            var a = Convert.ToInt32(Console.ReadLine());
            var b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine((a & ~b) | (~a & b));
            Console.ReadKey(true);
        }
    }
}
