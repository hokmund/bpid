﻿using System.Text;

namespace BPiD
{
    public class CaesarCipher
    {
        private const int LettersInAlphabet = 26;

        private readonly int shift;

        public CaesarCipher(int shift)
        {
            this.shift = shift;
        }

        public string Encrypt(string rawText)
        {
            var upperCaseText = rawText.ToUpper();
            var sb = new StringBuilder();
            foreach (var letter in upperCaseText)
            {
                var code = (letter - 'A' + shift) % LettersInAlphabet;
                sb.Append((char)('A' + code));
            }

            return sb.ToString();
        }

        public string Decrypt(string encryptedText)
        {
            var upperCaseText = encryptedText.ToUpper();
            var sb = new StringBuilder();
            foreach (var letter in upperCaseText)
            {
                var code = letter - 'A' - shift;

                if (code < 0)
                {
                    code += LettersInAlphabet;
                }

                sb.Append((char)('A' + code));
            }

            return sb.ToString();
        }
    }
}
