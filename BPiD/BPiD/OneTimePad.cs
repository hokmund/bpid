﻿using System;

namespace BPiD
{
    public class OneTimePad
    {
        private readonly byte[] privateKey = new byte[2048];

        public OneTimePad()
        {
            new Random().NextBytes(privateKey);
        }

        public OneTimePad(string privateKey)
        {
            var length = this.privateKey.Length / sizeof(char);

            if (privateKey.Length < length)
            {
                length = privateKey.Length;
            }

            Buffer.BlockCopy(privateKey.ToCharArray(), 0, this.privateKey, 0, length);
        }

        public void PrintKey()
        {
            Console.WriteLine($"Key: {GetString(privateKey)}");
        }

        public string Encrypt(string data)
        {
            return GetString(Xor(GetBytes(data)));
        }

        public string Decrypt(string data)
        {
            return GetString(Xor(GetBytes(data)));
        }

        private byte[] Xor(byte[] data)
        {
            var result = new byte[data.Length];

            for (var i = 0; i < data.Length; i++)
            {
                result[i] = (byte)(data[i] ^ privateKey[i]);
            }

            return result;
        }

        private static byte[] GetBytes(string str)
        {
            var bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        private static string GetString(byte[] bytes)
        {
            var chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
