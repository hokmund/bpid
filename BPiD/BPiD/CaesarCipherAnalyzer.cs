﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BPiD
{
    // Disclaimer: I love LINQ and I use it everywhere because it's so cute and functional-styled.
    // So don't blame me for readability.
    public class CaesarCipherAnalyzer
    {
        private const int LettersInAlphabet = 26;
        private const int KTop = 4;

        // Iterate over possible decryptions and select five best according to chi-square criterion.
        public void DecypherBruteforce(string text)
        {
            var sampleFrequencyDictionary = FrequencyDictionary.CreateFromFiles("../../Data/fiction");
            var results = new List<DecryptionResult>();

            for (var i = 0; i < LettersInAlphabet; ++i)
            {
                var caesarCipher = new CaesarCipher(i);
                var guess = caesarCipher.Decrypt(text);
                var frequencyDictionary = FrequencyDictionary.CreateFromCorpus(guess);

                var likelihoodRatio = PearsonsChiSquareTest(sampleFrequencyDictionary, frequencyDictionary);
                results.Add(new DecryptionResult
                {
                    Guess = guess,
                    Shift = i,
                    Confidence = likelihoodRatio
                });
            }

            Console.WriteLine("\nTop-5 results according to bruteforce with chi-square criterion:");
            foreach (var result in results.OrderBy(x => x.Confidence).Take(5))
            {
                Console.WriteLine($"{result.Guess} with {result.Shift} shift");
            }
        }

        // Algo from lecture.
        public void DecypherWithTopK(string text)
        {
            var sampleFrequencyDictionary = FrequencyDictionary.CreateFromFiles("../../Data/fiction");
            var frequencyDictionary = FrequencyDictionary.CreateFromCorpus(text);

            var topKSample = sampleFrequencyDictionary.OrderByDescending(x => sampleFrequencyDictionary[x]).Take(KTop).ToList();
            var topKTarget = frequencyDictionary.OrderByDescending(x => frequencyDictionary[x]).Take(KTop).ToList();

            var shifts = new Dictionary<int, int>();
            for (var i = 0; i < LettersInAlphabet; ++i)
            {
                shifts.Add(i, 0);
            }

            foreach (var letter in topKTarget)
            {
                foreach (var originalLetter in topKSample)
                {
                    var shift = letter - originalLetter;

                    if (shift < 0)
                    {
                        shift += 26;
                    }

                    ++shifts[shift];
                }
            }

            var results = shifts
                .Where(x => x.Value > 0)
                .OrderByDescending(x => x.Value)
                .Select(x => new DecryptionResult
                {
                    Guess = new CaesarCipher(x.Key).Decrypt(text),
                    Shift = x.Key
                });

            Console.WriteLine($"\nResults according to k most frequent letters (k = {KTop}):");
            foreach (var result in results)
            {
                Console.WriteLine($"{result.Guess} with {result.Shift} shift");
            }
        }

        private static double PearsonsChiSquareTest(FrequencyDictionary sample, FrequencyDictionary target)
        {
            var chiSquare = target.Count * Enumerable.Range(0, LettersInAlphabet)
                                   .Select(x => (char)('A' + x))
                                   .Sum(x => Math.Pow(target[x] - sample[x], 2) / sample[x]);

            return chiSquare;
        }

        private class DecryptionResult
        {
            public string Guess { get; set; }

            public int Shift { get; set; }

            public double Confidence { get; set; }
        }
    }
}
