﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BPiD
{
    public class FrequencyDictionary : IEnumerable<char>
    {
        private readonly Dictionary<char, int> frequencyDictionary = new Dictionary<char, int>();

        private FrequencyDictionary()
        {
        }

        public int Count { get; private set; }

        public int this[char c] => frequencyDictionary.ContainsKey(c) ? frequencyDictionary[c] : 0;

        public IEnumerator<char> GetEnumerator()
        {
            return frequencyDictionary.Keys.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            var countVector = this.GetCountVector();
            var sb = new StringBuilder();

            foreach (var frequencyPair in countVector)
            {
                sb.AppendLine($"{frequencyPair.Character}: {frequencyPair.Percentage}");
            }

            return sb.ToString();
        }

        public static FrequencyDictionary CreateFromFiles(string pathToCorpus)
        {
            var dictionary = new FrequencyDictionary();
            var files = Directory.GetFiles(pathToCorpus);

            for (int i = 'A'; i <= 'Z'; ++i)
            {
                dictionary.frequencyDictionary[(char)i] = 0;
            }

            foreach (var file in files)
            {
                using (var sr = new StreamReader(file))
                {
                    int code;
                    while ((code = sr.Read()) > 0)
                    {
                        var character = char.ToUpper((char)code);

                        if (IsLetter(character))
                        {
                            ++dictionary.frequencyDictionary[character];
                        }
                    }
                }
            }

            dictionary.Count = dictionary.frequencyDictionary.Sum(x => x.Value);

            return dictionary;
        }

        public static FrequencyDictionary CreateFromCorpus(string corpus)
        {
            var preprocessedCorpus = corpus.ToUpper();
            var dictionary = new FrequencyDictionary();

            for (int i = 'A'; i <= 'Z'; ++i)
            {
                dictionary.frequencyDictionary[(char)i] = 0;
            }

            foreach (var character in preprocessedCorpus)
            {
                if (IsLetter(character))
                {
                    ++dictionary.frequencyDictionary[character];
                }
            }

            dictionary.Count = dictionary.frequencyDictionary.Sum(x => x.Value);

            return dictionary;
        }

        private IEnumerable<Frequency> GetCountVector()
        {
            return frequencyDictionary.Select(x => new Frequency
            {
                Character = x.Key,
                Percentage = (double)x.Value / this.Count
            }).OrderByDescending(x => x.Percentage);
        }

        private static bool IsLetter(char character)
        {
            return character >= 'A' && character <= 'Z';
        }

        private class Frequency
        {
            public char Character { get; set; }

            public double Percentage { get; set; }
        }
    }
}
